Spire.DataExport
================
---------------------------------------------------------------------------------------------------------------------
This is a package of C#, VB.NET Example Projects for Spire.DataExport for .NET

Spire.DataExport for .NET is a .NET component which is designed to perform data exporting processing tasks.Spire.DataExport  supports to export data to MS Word, MS Excel, MS Access, PDF, Text, HTML, XML, RTF, DBF, SQL Script, SYLK, CSV, DIF , MS Clipboard format. Besides, this data export component enables you to easily export data from ListView, Command and DataTable components.

Using Spre.DataExport for .NET, you can benefit:

1. Multiple formats available

2. Totally independent component

3. Rich options when exporting data
 
4. ASP.NET and Windows Forms support

5. Multi-language support

6. Scalability and Usability

7. Easy manipulation and high fidelity

How to Install?
============
_______________________________________________________________________________________________________________________

Spire.DataExport dll is independent with sample projects. They are all packed in new versions, but for hotfix version, there is only Spire.DataExport dll. For a new user of Spire.DataExport for .NET, it is better to keep the sample projects in it. But if you have used it, you can only extract the Spire.DataExport dll to any location as you like without any running error.

How to Run Project Demos?
===========
_______________________________________________________________________________________________________________________

There is the default project in the sample project pack. Often the first sample project is the default project. So when you run the demo by pressing “F5” or clicking the “Debug”, it actually debugs the first project. The best way is to set the project you want to debug as the StartUp Project. So be sure you are running the right project.

Customer Driven Service
================
________________________________________________________________________________________________________________________

As for our customers, we are devoted to satisfying every customer need. We give customers whole right to review the product, then, constantly to better and develop it according to customer feel, which will be shown below:

1.Features will be optimized according to your requirements.

2.Frequently release new version to add new features or fix bugs.

3.Responsible free technical support for anyone within at most one business day.

4.Online support of professional technical advisers in communities and forums.


